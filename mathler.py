import random

class Expression():
    def __init__(self, string):
        self.string = string.replace("[","(").replace("]",")")

    def __str__(self):
        return self.string

    def __repr__(self):
        return "<Exp {}>".format(str(self))

    @staticmethod
    def sanitize(string):
        i = string.find("(", 1)
        while i > 0:
            if string[i-1] in "0123456789)":
                string = string[:i] + "*" + string[i:]
            i = string.find("(", i+1)

        i = string.find(")", 0)
        while i > 0:
            if string[i+1] in "0123456789(":
                string = string[:i+1] + "*" + string[i+1:]
            i = string.find(")", i+1)
        return string

    @staticmethod
    def add(l_exp, r_exp):
        return l_exp + r_exp

    @staticmethod
    def sub(l_exp, r_exp):
        return l_exp - r_exp

    @staticmethod
    def mul(l_exp, r_exp):
        return l_exp * r_exp

    @staticmethod
    def div(l_exp, r_exp):
        return l_exp / r_exp

    @staticmethod
    def get_op_fn(op):
        return {
            "+": Expression.add,
            "-": Expression.sub,
            "*": Expression.mul,
            "/": Expression.div,
        }.get(op)

    def eval(self):
        i = self.string.find("(")
        if i >= 0:
            depth = 1
            j = i
            while depth > 0:
                j += 1
                if self.string[j] == "(":
                    depth += 1
                elif self.string[j] == ")":
                    depth -= 1
            result = Expression(
                "".join([
                    self.string[:i],
                    "{:104.52f}".format(Expression(self.string[i+1:j]).eval()).strip(),
                    self.string[j+1:],
                ])
            ).eval()
            return result

        for token in "+-*/":
            i = self.string.rfind(token)
            while i > 0 and token == "-" and self.string[i-1] in "+-*/":
                i -= 1
            if i > 0:
                result = Expression.get_op_fn(token)(
                    Expression(self.string[:i]).eval(),
                    Expression(self.string[i+1:]).eval(),
                )
                return result

        result_float = float(self.string)
        try:
            result_int = int(self.string)
            if result_int == result_float:
                return result_int
        except:
            return result_float


class Symbol():
    def __init__(self, value=None):
        self.value = value

    def copy(self):
        return Symbol(self.value)

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return "Symbol <{}>".format(self.value)

    def is_fixed(self):
        return self.value is not None


class Guesser():
    def __init__(self, goal, num_squares=6):
        self.goal = goal
        self.num_squares = num_squares
        self.symbol_pool = [Symbol(v) for v in "1234567890+-*/"]
        self.symbols = [Symbol() for i in range(self.num_squares)]
        self.guesses_made = []

    def concatenate_symbols(self, symbols):
        return "".join([str(s) for s in symbols])

    def evaluate(self, symbols):
        expression = self.concatenate_symbols(symbols)
        return Expression(Expression.sanitize(expression)).eval()

    def make_a_good_guess(self):
        best_last_guess = ""
        diligence = 1000
        while True:
            remaining_symbols = set(self.concatenate_symbols(self.symbol_pool))

            best_guess_symbols = set(best_last_guess)
            best_guess_new_symbols = best_guess_symbols.intersection(remaining_symbols)


            diligence -= len(best_guess_new_symbols)

            if diligence <= 0 or (len(best_guess_new_symbols) == self.num_squares and best_last_guess not in self.guesses_made):
                break
            new_guess = self.make_a_guess()

            new_guess_symbols = set(new_guess)
            new_guess_new_symbols = new_guess_symbols.intersection(remaining_symbols)

            if len(new_guess_new_symbols) > len(best_guess_new_symbols):
                best_last_guess = new_guess

            self.last_guess = best_last_guess

        remaining_symbols = "".join(sorted(list(remaining_symbols)))
        new_guess_symbols = "".join(sorted(list(best_guess_new_symbols)))
        print("remaining: '{}'".format(remaining_symbols))
        print("new guess contains: '{}'".format(new_guess_symbols))
        print("deligence: {}".format(diligence))

        self.guesses_made.append(self.last_guess)
        return self.last_guess

    def make_a_guess(self):
        guess = None
        while guess != self.goal:
            try:
                guess_symbols = [sym.copy() for sym in self.symbols]
                for i,symbol in enumerate(list(guess_symbols)):
                    if symbol.is_fixed():
                        continue
                    symbol_pool = self.symbol_pool if i > 0 else self.symbol_pool[:9]
                    guess_symbols[i] = random.choice(symbol_pool).copy()
                guess = self.evaluate(guess_symbols)
            except Exception as e:
                pass # next guess
        self.last_guess = self.concatenate_symbols(guess_symbols)
        print(self.last_guess)
        return self.last_guess

    def get_user_feedback_for_last_guess(self):
        print()
        print("Feedback for my last guess:")
        for i,symbol in enumerate(self.last_guess, 1):
            feedback = None

            while feedback not in [1,2,3]:
                inp = input("Symbol '{}' (#{}) <gray=1> <yellow=2> <gren=3>: ".format(symbol, i))
                try:
                    feedback = int(inp)
                except:
                    continue

            if feedback == 1:
                self.remove_symbol_from_symbol_pool(symbol)
            elif feedback == 3:
                self.make_symbol_fixed(i-1)

    def remove_symbol_from_symbol_pool(self, symbol):
        remove_symbol = None
        for i,sym in enumerate(self.symbol_pool):
            if sym.value == symbol:
                remove_symbol = sym
        if remove_symbol is not None:
            self.symbol_pool.remove(remove_symbol)

    def make_symbol_fixed(self, index):
        self.symbols[index] = Symbol(self.last_guess[index])

if __name__ == "__main__":

#    import sys
#    sanitized = Expression.sanitize(sys.argv[1])
#    print(Expression(sanitized).eval())

    goal = int(input("Goal: "))
    squares = int(input("Squares: "))
    guesser = Guesser(goal, squares)
    for i in range(6):
        if i > 0:
            guesser.get_user_feedback_for_last_guess()
        guess = guesser.make_a_good_guess()
        print("#"*80)
        print("### MY GUESS IS: '{}' ###".format(guess))
        print("#"*80)
